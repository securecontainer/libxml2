#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-Wl,-R/usr/pkg/lib -L/usr/pkg/lib"
XML2_LIBS="-lxml2  -lz -L/usr/pkg/lib -llzma  -L/usr/pkg/lib -liconv  -lm "
XML2_INCLUDEDIR="-I/usr/pkg/include/libxml2 -I/usr/pkg/include"
MODULE_VERSION="xml2-2.9.4"

